package com.example.tpsmallanounces.controllers;

import com.example.tpsmallanounces.services.UserService;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;

@Controller
@RequestMapping("user")
public class UserController {

    @Qualifier("userService")
    @Autowired
    private UserService _userService;
    @Autowired
    private HttpServletResponse _response;

    @GetMapping("register")
    public ModelAndView registerForm(){
        ModelAndView mv = new ModelAndView("register");
        return mv;
    }

    @PostMapping("register-submit")
    public ModelAndView submitRegister(@RequestParam String firstname, @RequestParam String lastname, @RequestParam String email, @RequestParam String phone, @RequestParam String password) throws IOException {
        ModelAndView mv = new ModelAndView("register");
        if(_userService.register(firstname, lastname, phone, email, password)){
            _response.sendRedirect("/user/login");
        }
        return mv;
    }

    @GetMapping("login")
    public ModelAndView loginForm(){
        ModelAndView mv = new ModelAndView("login");
        return mv;
    }

    @PostMapping("login-submit")
    public ModelAndView submitLogin(@RequestParam String email, @RequestParam String password) throws IOException {
        ModelAndView mv = new ModelAndView("login");
        if(_userService.login(email, password)){
            _response.sendRedirect("/home");
        }
        return mv;
    }
}
