package com.example.tpsmallanounces.controllers;

import com.example.tpsmallanounces.entities.Annonce;
import com.example.tpsmallanounces.services.AnnonceService;
import com.example.tpsmallanounces.services.UserService;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/home")
public class HomeController {

    @Autowired
    public AnnonceService _annonceService;

    @Autowired
    public HttpServletResponse _response;

    @Qualifier("userService")
    @Autowired
    public UserService _userService;

    @GetMapping("")
    public ModelAndView getAccueil() throws IOException {
        if (!_userService.isLogged()) {
            _response.sendRedirect("/user/login");
        }
        ModelAndView mv = new ModelAndView("home");
        List<Annonce> annonceList = new ArrayList<>();
        annonceList.addAll(_annonceService.getAll());
        mv.addObject(annonceList);
        return mv;
    }

    @PostMapping("/search")
    public ModelAndView getAnnoncesBySearch(@RequestParam String title) throws Exception {
        ModelAndView mv = new ModelAndView("home");
        if (title != null) {
            try {
                List<Annonce> annonces = _annonceService.searchByTitle(title);
                mv.addObject(annonces);
                return mv;
            } catch (Exception e) {
                throw new Exception(e);
            }
        }
        return null;
    }

    @GetMapping("favorite")
    public ModelAndView getFavorite() throws IOException {
        if (!_userService.isLogged()) {
            _response.sendRedirect("/user/login");
        }
        ModelAndView mv = new ModelAndView("favorite");
        List<Annonce> annonceList = new ArrayList<>(_annonceService.getFavorites());
        if (!annonceList.isEmpty())
            mv.addObject(annonceList);
        return mv;
    }


}
