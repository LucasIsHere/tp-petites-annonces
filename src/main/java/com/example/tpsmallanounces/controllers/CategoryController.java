package com.example.tpsmallanounces.controllers;

import com.example.tpsmallanounces.services.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("category")
public class CategoryController {

    @Autowired
    AdminService _adminService;

    @GetMapping("create")
    public ModelAndView getCategoryForm(){
        ModelAndView mv = new ModelAndView("category-form");
        return mv;
    }

    @PostMapping("submit-category")
    public ModelAndView submitCategory(@RequestParam String name){
        ModelAndView mv = new ModelAndView("category-form");
        if(name != null){
            _adminService.createCategory(name);
        }
        return mv;
    }
}
