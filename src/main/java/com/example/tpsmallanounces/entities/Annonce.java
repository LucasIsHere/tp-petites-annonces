package com.example.tpsmallanounces.entities;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Annonce {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String title;
    private String description;
    @Column(name = "date_creation")
    @Temporal(TemporalType.DATE)
    private Date dateCreation;
    private boolean status;
    private boolean favorite;
    @ManyToOne
    private User user;

    @OneToMany(mappedBy = "annonce")
    private List<Category> categories;

    @Cascade(CascadeType.ALL)
    @OneToMany(mappedBy = "annonce")
    private List<Image> images;

    public Annonce(String title, String description){
        this.title = title;
        this.description = description;
        this.dateCreation = new Date();
        this.status = false;
        this.images = new ArrayList<>();
        this.categories = new ArrayList<>();
    }
}
