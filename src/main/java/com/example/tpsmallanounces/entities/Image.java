package com.example.tpsmallanounces.entities;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
public class Image {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String url;
    @ManyToOne
    private Annonce annonce;

}
