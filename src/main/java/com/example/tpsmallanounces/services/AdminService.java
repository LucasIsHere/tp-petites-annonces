package com.example.tpsmallanounces.services;

import com.example.tpsmallanounces.entities.Annonce;
import com.example.tpsmallanounces.entities.Category;
import com.example.tpsmallanounces.entities.User;
import com.example.tpsmallanounces.repositories.AnnonceRepository;
import com.example.tpsmallanounces.repositories.CategoryRepository;
import com.example.tpsmallanounces.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminService extends UserService {

    @Autowired
    public AnnonceRepository _annonceRepository;

    @Autowired
    public UserRepository _userRepository;

    @Autowired
    public CategoryRepository _categoryRepository;

    public boolean deleteUser(int id){
        User user = _userRepository.findUserById(id);
        if(user != null){
            _userRepository.delete(user);
            return true;
        }
        return false;
    }

    public Category createCategory(String name){
        if(name != null){
            Category category = new Category();
            category.setName(name);
            _categoryRepository.save(category);
            return category;
        }
        return null;
    }

    public boolean deleteAnnonce(int id){
        Annonce annonce = _annonceRepository.findAnnonceById(id);
        if(annonce != null){
            _annonceRepository.delete(annonce);
            return true;
        }else{
            return false;
        }
    }


}
