package com.example.tpsmallanounces.services;

import com.example.tpsmallanounces.entities.Annonce;
import com.example.tpsmallanounces.entities.Category;
import com.example.tpsmallanounces.entities.Image;
import com.example.tpsmallanounces.repositories.AnnonceRepository;
import com.example.tpsmallanounces.repositories.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class AnnonceService {

    @Autowired
    public AnnonceRepository _annonceRepository;

    @Autowired
    private UploadService uploadService;

    @Autowired
    private ImageRepository _imageRepository;

    public Annonce create(String title, String description) throws Exception {
        if (title == null || description == null) {
            throw new Exception("Veuillez remplir la totalité des champs");
        }
        Annonce annonce = new Annonce(title, description);
        _annonceRepository.save(annonce);
        return annonce;
    }

    public Annonce create(String title, String description, List<MultipartFile> images, List<Category> categories) throws Exception {
        if (title == null || description == null) {
            throw new Exception("Veuillez remplir la totalité des champs");
        }
        Annonce annonce = new Annonce(title, description);
        if (_annonceRepository.save(annonce) != null) {
            for (MultipartFile img : images) {
                Image image = new Image();
                image.setUrl(uploadService.store(img));
                image.setAnnonce(annonce);
                _imageRepository.save(image);
                annonce.getImages().add(image);
            }
            for (Category c : categories
            ) {
                annonce.getCategories().add(c);
            }
            return annonce;
        }
        return null;
    }


    public boolean delete(int id) {
        Date date = new Date();
        Annonce annonce = _annonceRepository.findAnnonceById(id);
        if (date.getMonth() == annonce.getDateCreation().getMonth() + 3) {
            _annonceRepository.delete(annonce);
        }
        if (annonce != null) {
            _annonceRepository.delete(annonce);
            return true;
        }
        return false;
    }

    public List<Annonce> searchByTitle(String title) throws Exception {
        if (title == null) {
            throw new Exception("Veuillez remplir le champ.");
        }
        List<Annonce> annonces = _annonceRepository.findAnnonceByTitleContaining(title);
        return annonces;
    }

    public boolean updateToFavorite(int id) throws Exception {
        Annonce annonce = _annonceRepository.findAnnonceById(id);
        if (annonce == null) {
            throw new Exception("Aucune annonce avec cet id.");
        } else {
            annonce.setFavorite(!annonce.isFavorite());
            _annonceRepository.save(annonce);
            return true;
        }
    }

    public List<Annonce> getAll() {
        List<Annonce> annonces = new ArrayList<>();
        _annonceRepository.findAll().forEach(annonce -> annonces.add(annonce));
        return annonces;
    }

    public List<Annonce> getFavorites() {
        return new ArrayList<>(_annonceRepository.findAnnonceByFavorite(true));
    }
}
