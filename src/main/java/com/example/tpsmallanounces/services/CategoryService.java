package com.example.tpsmallanounces.services;

import com.example.tpsmallanounces.entities.Annonce;
import com.example.tpsmallanounces.entities.Category;
import com.example.tpsmallanounces.repositories.CategoryRepository;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryService {

    @Autowired
    public CategoryRepository _categoryRepository;

    public List<Category> getAll(){
        List<Category> categories = new ArrayList<>();
        _categoryRepository.findAll().forEach(category -> categories.add(category));
        return categories;
    }
}
