package com.example.tpsmallanounces.services;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface UploadService {
    String store(MultipartFile file) throws IOException;

}
