package com.example.tpsmallanounces.repositories;

import com.example.tpsmallanounces.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

    public User searchUserByEmail(String email);

    public User findUserById(int id);

}
